#define _DEFAULT_SOURCE
#include "tests.h"

bool block_is_free(void* block){
	struct block_header* header = block - offsetof(struct block_header, contents);
	return header->is_free;
}

bool test_1() {
  printf("Тест 1. Обычное успешное выделение памяти.\n");
	
  void* heap = heap_init(HEAP_SIZE);

  void* alloc = _malloc(ALLOC_SIZE);
  if (!alloc){
		fprintf( stderr, "Ошибка аллокации.\n");
		return false;
  }
	
  debug_heap(stdout, HEAP_START);
  _free(alloc);

  munmap(heap, HEAP_SIZE);
  return true;
}

bool test_2() {
  printf("Тест 2. Освобождение одного блока из нескольких выделенных.\n");
	
  void* heap = heap_init(HEAP_SIZE);

  void* alloc_1 = _malloc(ALLOC_SIZE);
  void* alloc_2 = _malloc(ALLOC_SIZE);
  void* alloc_3 = _malloc(ALLOC_SIZE);
	
  if (!alloc_1 || !alloc_2 || !alloc_3){
		fprintf( stderr, "Ошибка аллокации.\n");
		return false;
  }
	
  debug_heap(stdout, HEAP_START);

  _free(alloc_2);

  if (!block_is_free(alloc_2)){
    fprintf( stderr, "Ошибка освобождения памяти: Не удалось освободить блок.\n");
    return false;
  }

  if (block_is_free(alloc_1) || block_is_free(alloc_3)) {
      fprintf( stderr, "Ошибка освобождения памяти: Затронуты соседние блоки.\n");
      return false;
  }

  debug_heap(stdout, HEAP_START);
	
  _free(alloc_1);
  _free(alloc_3);

  munmap(heap, HEAP_SIZE);
  return true;
}

bool test_3() {
  printf("Тест 3. Освобождение двух блоков из нескольких выделенных.\n");
	
  void* heap = heap_init(HEAP_SIZE);

  void* alloc_1 = _malloc(ALLOC_SIZE);
  void* alloc_2 = _malloc(ALLOC_SIZE);
  void* alloc_3 = _malloc(ALLOC_SIZE);
  void* alloc_4 = _malloc(ALLOC_SIZE);
	
  if (!alloc_1 || !alloc_2 || !alloc_3 || !alloc_4){
		fprintf( stderr, "Ошибка аллокации.\n");
		return false;
  }
	
  debug_heap(stdout, HEAP_START);

  _free(alloc_2);
  _free(alloc_3);

  if (!block_is_free(alloc_2) || !block_is_free(alloc_3)){
    fprintf( stderr, "Ошибка освобождения памяти: Не удалось освободить блок.\n");
    return false;
  }

  if (block_is_free(alloc_1) || block_is_free(alloc_4)) {
      fprintf( stderr, "Ошибка освобождения памяти: Затронуты соседние блоки.\n");
      return false;
  }

  debug_heap(stdout, HEAP_START);

  _free(alloc_1);
  _free(alloc_4);

  munmap(heap, HEAP_SIZE);
  return true;
}

bool test_4() {
	printf("Тест 4. Память закончилась, новый регион памяти расширяет старый.\n");

	void* heap = heap_init(HEAP_SIZE);

  void* alloc = _malloc(HEAP_SIZE * 2);
	if (!alloc){
		fprintf( stderr, "Ошибка аллокации.\n");
		return false;
	}

  debug_heap(stdout, heap);

  if (alloc != heap + offsetof(struct block_header, contents)){
		fprintf( stderr, "Ошибка расширения памяти: Новый регион памяти не расширяет старый.\n");
    return false;
	}
  
  _free(alloc);

  munmap(heap, HEAP_SIZE * 2);
	return true;
}

bool test_5() {
	printf("Тест 5. Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.\n");
	
	void* heap = heap_init(HEAP_SIZE);
	
	void* alloc_1 = _malloc(ALLOC_SIZE);
	if (!alloc_1){
		fprintf( stderr, "Ошибка аллокации первого блока.\n");
		return false;
	}
	
	void* new_block = mmap(heap + HEAP_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);
  if (new_block == MAP_FAILED) {
    fprintf( stderr, "Ошибка выделения памяти после кучи.\n");
		return false;
  }
	
	void* alloc_2 = _malloc(ALLOC_SIZE);
	if (!alloc_2){
		fprintf( stderr, "Ошибка аллокации второго блока.\n");
		return false;
	}
	
	if (alloc_2 == alloc_1 + offsetof(struct block_header, contents)){
		fprintf( stderr, "Ошибка расширения памяти: Новый регион памяти расширяет старый.\n");
    return false;
	}
  
  _free(alloc_1);
  _free(alloc_2);

  munmap(heap, HEAP_SIZE);
	munmap(new_block, REGION_MIN_SIZE);

	return true;
}

void do_tests(){
  bool res;

	res = test_1();
	printf("test 1 %s \n", res ? "passed" : "failed");
	
	res = test_2();
	printf("test 1 %s \n", res ? "passed" : "failed");

	res = test_3();
	printf("test 1 %s \n", res ? "passed" : "failed");

	res = test_4();
	printf("test 1 %s \n", res ? "passed" : "failed");

	res = test_5();
	printf("test 1 %s \n", res ? "passed" : "failed");
}




