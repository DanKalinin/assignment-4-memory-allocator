#ifndef TESTS_H
#define TESTS_H

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#define HEAP_SIZE (2 * 4096)
#define ALLOC_SIZE 2048

void do_tests();

#endif

